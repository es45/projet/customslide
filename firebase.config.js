// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyD5UXxHp0_lNuKZWHxMBACR4xhjJ6mFljo",
  authDomain: "projetpwa-e6ed1.firebaseapp.com",
  projectId: "projetpwa-e6ed1",
  storageBucket: "projetpwa-e6ed1.appspot.com",
  messagingSenderId: "531680072796",
  appId: "1:531680072796:web:7b83f73e3830d45ebb8299",
  databaseURL: "https://projetpwa-e6ed1-default-rtdb.europe-west1.firebasedatabase.app/"
};

// // Initialize Firebase
const app = initializeApp(firebaseConfig);

const database = getDatabase(app);

export default app;
