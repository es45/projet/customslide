import Vue from "vue";
import Vuex from "vuex";
import userStore from "@/store/user.store";
import slideStore from "@/store/slide.store";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: { user: userStore, slide: slideStore },
});
