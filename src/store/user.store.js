import { signOut, getAuth, setPersistence, browserSessionPersistence, signInWithEmailAndPassword, createUserWithEmailAndPassword, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import provider from "@/firebase/google_provider";

const state = {
    auth: {}
}

const getters = {
    isAuth: (state) => {
        return Object.keys(state.auth).length > 0;
    },
    user: (state) => {
        return state.auth;
    }
}

const mutations = {
    LOGIN(state, res) {
        state.auth = res;
    },
    LOGOUT(state) {
        state.auth = {}
    }
}

const actions = {
    async loginGoogle({commit}, payload) {
        try {
            const auth = getAuth();
            signInWithPopup(auth, provider)
                .then((result) => {
                    // This gives you a Google Access Token. You can use it to access the Google API.
                    const credential = GoogleAuthProvider.credentialFromResult(result);
                    const token = credential.accessToken;
                    // The signed-in user info.
                    const user = result.user;
                    commit('LOGIN', user);
                    // ...
                }).catch((error) => {
                // Handle Errors here.
                const errorCode = error.code;
                const errorMessage = error.message;
                // The email of the user's account used.
                const email = error.customData.email;
                // The AuthCredential type that was used.
                const credential = GoogleAuthProvider.credentialFromError(error);
                // ...
            });
        } catch (e) {

        }
    },
    async login({ commit }, payload) {
        const auth = getAuth();
        setPersistence(auth, browserSessionPersistence)
            .then(() => {
                return signInWithEmailAndPassword(auth, payload.email, payload.password).then((userCredential) => {
                    // Signed in
                    const user = userCredential.user;
                    commit('LOGIN', user);
                })
                    .catch((error) => {
                        const errorCode = error.code;
                        const errorMessage = error.message;
                    });
            })
            .catch((error) => {
                // Handle Errors here.
                const errorCode = error.code;
                const errorMessage = error.message;
            });
    },
    async signup({ commit }, payload) {
        const auth = getAuth();
        setPersistence(auth, browserSessionPersistence)
            .then(() => {
                return createUserWithEmailAndPassword(auth, payload.email, payload.password).then((userCredential) => {
                    // Signed in
                    const user = userCredential.user;
                    commit('LOGIN', user);
                })
                .catch((error) => {
                    const errorCode = error.code;
                    const errorMessage = error.message;
                    // ..
                });
            })
            .catch((error) => {
                // Handle Errors here.
                const errorCode = error.code;
                const errorMessage = error.message;
            });
    },
    async logout({commit}) {
        try {
            const auth = getAuth();
            signOut(auth).then(() => {
                commit('LOGOUT')
            }).catch((error) => {
                // An error happened.
            });
        } catch (e) {
            console.error(e)
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
