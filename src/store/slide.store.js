import { getDatabase, ref, set, push, onChildAdded, onValue, update, remove } from "firebase/database";
import store from "@/store/index";

const state = {
    presentations: {}
}
const getters = {
    presentations: (state) => {
        return state.presentations;
    },
    presentationTitle: (state) => (presentationId) => {
        return state.presentations[presentationId]?.title;
    },
    getFirstSlide: (state) => (presentationId) => {
        const firstSlideId = Object.keys(state.presentations[presentationId].slides)[0];
        return state.presentations[presentationId].slides[firstSlideId].id
    }
}

const mutations = {
    setPresentations: (state, presentations) => {
        state.presentations = presentations;
    },
    addPresentation: (state, presentation) => {
        state.presentations = presentation;
    },
    addSlide: (state, slide) => {
    },
    updateSlide: (state, slide) => {
    },
    updatePresentation: (state, val) => {
    }
}

const actions = {
    async getPresentations({ commit }, payload) {
        try {
            const db = getDatabase();
            const presentations = ref(db, 'users/' + payload.userId + '/presentations');
            onValue(presentations, (snapshot) => {
                commit("setPresentations", snapshot.val());
            });
        } catch (e) {
            console.error(e);
        }
    },
    async addPresentation({ commit, dispatch }, payload) {
        try {
            const db = getDatabase();
            const presentationRef = ref(db, 'users/' + payload.userId + '/presentations');
            const newPresentationRef = push(presentationRef);
            set(newPresentationRef, {
                title: payload.title,
                id: newPresentationRef.key
            });
            dispatch('addSlide', newPresentationRef.key);
            onValue(presentationRef, (snapshot) => {
                commit("addPresentation", snapshot.val());
            });
        } catch (e) {
            console.error(e);
        }
    },
    async updatePresentation({ commit }, payload) {
        try {
            const db = getDatabase();
            const presentationRef = ref(db, 'users/' + payload.userId + '/presentations/' + payload.presentationId);
            update(presentationRef, {
                title: payload.title,
            });
            onValue(presentationRef, (snapshot) => {
                commit("updatePresentation", snapshot.val());
            });
        } catch (e) {
            console.error(e);
        }
    },
    async addSlide({ commit }, presentationId) {
        try {
            const db = getDatabase();
            const slideRef = ref(db, 'users/' + store.getters['user/user'].uid + '/presentations/' + presentationId + '/slides');
            const newSlideRef = push(slideRef);
            set(newSlideRef, {
                content: '',
                id: newSlideRef.key
            });
            onValue(newSlideRef, (snapshot) => {
                commit("addSlide", snapshot.val());
            });
        } catch (e) {
            console.error(e);
        }
    },
    async updateSlide({ commit }, payload) {
        try {
            const db = getDatabase();
            const slideRef = ref(db, 'users/' + store.getters['user/user'].uid + '/presentations/' + payload.presentationId + '/slides/' + payload.slideId);
            set(slideRef, {
                content: payload.value,
                id: slideRef.key
            });
            onValue(slideRef, (snapshot) => {
                commit("updateSlide", snapshot.val());
            });
        } catch (e) {
            console.error(e);
        }
    },
    async deleteSlide({ commit }, payload) {
        try {
            const db = getDatabase();
            const slideRef = ref(db, 'users/' + store.getters['user/user'].uid + '/presentations/' + payload.presentationId + '/slides/' + payload.slideId);
            remove(slideRef);
        } catch (e) {
            console.error(e);
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
