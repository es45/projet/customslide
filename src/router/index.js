import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "../views/LoginView.vue";
import Signup from "../views/Signup.vue";
import PresentationView from "../views/PresentationView";
import PresentationMode from "../views/PresentationMode";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "home",
        component: HomeView,
    },
    {
        path: "/login",
        name: "login",
        component: LoginView,
    },
    {
        path: "/signup",
        name: "signup",
        component: Signup,
    },
    {
        path: "/presentation/:id/:slideId",
        name: "presentationSlides",
        component: PresentationView,
        meta: { isAuth: true },
        props: true,
    },
    {
        path: "/presentation/:id",
        name: "presentation",
        component: PresentationMode,
        meta: { isAuth: true },
        props: true,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

// router.beforeEach((to, from, next) => {
//     // console.log(store)
//     if(to?.meta?.isAuth && store.getters['user/isAuth']) {
//         next();
//     } else {
//         return {
//             path: '/login',
//             query: { redirect: to.fullPath },
//         }
//     }
// })

export default router;
