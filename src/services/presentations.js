import store from "@/store";

export const getPresentations = () => {
    return store.getters["slide/presentations"];
};

export const getPresentationSlides = (id) => {
    const presentations = getPresentations();
    return presentations[id]?.slides;
};

export const getContentOfSlide = (idPresentation, idSlide) => {
    const presentations = getPresentations();
    const slides = presentations[idPresentation]?.slides;
    if (slides) {
        return slides[idSlide]?.content;
    }
};

export const formatToRevealJS = (idPresentation) => {
    let formatedHTML = [];
    let presentations = getPresentations();
    let currentPresentation = presentations[idPresentation];
    for (const [key, value] of Object.entries(currentPresentation.slides)) {
        formatedHTML.push("<section>" + currentPresentation.slides[key].content + "</section>")
    }
    return formatedHTML.join("");
};

export const checkRoutePath = (path) => {
    const regex = /presentation\/...+/g;
    return regex.test(path);
};

export const getPresentationIdFromPath = (path) => {
    return path.split("/")[2];
};
