import { GoogleAuthProvider } from "firebase/auth";

const provider = new GoogleAuthProvider();

provider.setCustomParameters({
    'login_hint': 'user@example.com'
});

export default provider;
